public class Products {

    public static Product[] vegetables = {
            new Product("Carrots", 0.55f),
            new Product("Potatoes", 1.55f)
    };
    public static Product[] fruits = {
            new Product("Apples", 0.15f),
            new Product("Mango", 3.00f),
            new Product("Oranges", 1.95f)
    };
    public static Product[] dairy = {
            new Product( "Milk", 1.10f),
            new Product("Cheese", 3.50f)
    };
}
