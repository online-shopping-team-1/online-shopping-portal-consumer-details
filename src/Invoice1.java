import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Invoice1 {
    Basket basket;
    DeliveryInfo deliveryInfo;

    public Invoice1(Basket basket, DeliveryInfo deliveryInfo) {
        this.basket = basket;
        this.deliveryInfo = deliveryInfo;
    }

    public void printInvoice(){

        System.out.println(Main.ANSI_RESET + "Your Invoice");
        Date now = new Date();
        DecimalFormat df = new DecimalFormat("##.##");
        System.out.println("Date of the order: " + now);
        System.out.println(basket.getBasketTextToPrint());

        double salesTax = basket.totalSum * (.21);
        System.out.println("Vat 21%: " + df.format(salesTax));

        double grandTotalTaxed = basket.totalSum + salesTax;
        System.out.println("Sum with Vat 21%:  " + df.format(grandTotalTaxed));
        System.out.println(" ");

        double finalSum = grandTotalTaxed - SmallLotteryGame.startLotteryGame();
        System.out.println(Main.ANSI_RESET + "Total to pay including Gift card  : " + df.format(finalSum));

        System.out.println("___________________________________________________________________");
        System.out.println(Main.ANSI_BLUE + "DELIVERY INFORMATION:");
        System.out.println("Address: " + deliveryInfo.deliveryCity + ", " + deliveryInfo.deliveryAddress);
        System.out.println("Contact person info: " + deliveryInfo.contactPerson + ", Phone No. " + deliveryInfo.phoneNo);
        //  Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow = cal.getTime();

        System.out.println("Delivery date (the latest time You'll receive Your order: " + tomorrow);
        System.out.println(Main.ANSI_WHITE + "Please find your Invoice in a pop up window to view full purchase details!" + Main.ANSI_RESET);


        JFrame frame = new JFrame("*****INVOICE*****");
        frame.setBackground(Color.PINK);
        frame.setPreferredSize(new Dimension(900, 800));
        frame.pack();

        JTextArea textArea = new JTextArea("Date of the order: " + now);
        textArea.append("\n\t\tINVOICE");
        textArea.append("\n_________________________________________________________________\n");
        textArea.setFont(new Font("Verdana", Font.PLAIN, 18));
        textArea.setBackground(Color.PINK);
        textArea.append(basket.getBasketTextToPrint());
        textArea.append("\n");
        textArea.append("Vat 21%: " + df.format(salesTax) + "\n");
        textArea.append("Sum with Vat 21%:  " + df.format(grandTotalTaxed) + "\n");
        textArea.append("Total to pay including Gift card  : " + df.format(finalSum));
        textArea.append("\n_________________________________________________________________\n");
        textArea.append("DELIVERY INFORMATION: " + deliveryInfo.deliveryCity + ", " + deliveryInfo.deliveryAddress + "\n");
        textArea.append("CONTACT PERSON INFORMATION.: " + deliveryInfo.contactPerson + ", Phone No. " + deliveryInfo.phoneNo + "\n");
        textArea.append("DELIVERY DATE (the latest time You'll receive Your order): " + tomorrow);
        textArea.append("\n_________________________________________________________________\n");
        textArea.append("\n\tTHANK YOU FOR SHOPPING WITH US!");

        frame.add(textArea);
        frame.setVisible(true);


        Scanner input = new Scanner(System.in);

        System.out.println(Main.ANSI_PURPLE + "Do you want to continue shopping?: ");
        System.out.println("Enter " + Main.ANSI_GREEN + "'YES'" + Main.ANSI_RESET + " to continue shopping: ");
        System.out.println("Enter " + Main.ANSI_YELLOW + "'NO'" + Main.ANSI_RESET + " to Exit program: ");
        String inputAnswer = input.next();
        if (inputAnswer.equalsIgnoreCase("YES")) {
            Basket basket = new Basket();
            basket.startProductSelection();
            System.out.println(" ");
            DeliveryInfo deliveryInfo = new DeliveryInfo();
            deliveryInfo.startDeliveryInfo();
            Invoice1 invoice = new Invoice1(basket, deliveryInfo);
            invoice.printInvoice();
        } else {
            System.out.println("Good buy! Have a nice day!");
            System.exit(0);
        }
    }
}

