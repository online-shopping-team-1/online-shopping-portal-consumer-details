import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Basket {
    public ArrayList<Purchase> purchaseArrayList = new ArrayList<>();
    public float totalSum;

    public void startProductSelection() { //This can be later moved to a seperate class
        System.out.println(Main.ANSI_GREEN + "1. Vegetables" + Main.ANSI_RED + " | 2. Fruits | " + Main.ANSI_YELLOW + " 3. Dairy ..."); //Possibly in table later
        Scanner input1 = new Scanner(System.in);
        System.out.println(Main.ANSI_RESET + "Please choose category: ");
        String productCategory = input1.nextLine();

        while ((!productCategory.equalsIgnoreCase("vegetables")) && (!productCategory.equalsIgnoreCase("fruits")) && (!productCategory.equalsIgnoreCase("dairy")) && (!productCategory.equals("3.")) && (!productCategory.equals("3")) && (!productCategory.equals("2.")) && (!productCategory.equals("2")) && (!productCategory.equals("1.")) && (!productCategory.equals("1"))) {
            System.out.println(Main.ANSI_RED + "Category not found!");
            System.out.println(Main.ANSI_RESET + "Please choose category: ");
            productCategory = input1.next();
        }
        if (productCategory.equals("1.") || productCategory.equals("1") || productCategory.equalsIgnoreCase("vegetables")) {
            chooseProductFromArray(Products.vegetables);
        } else if (productCategory.equals("2.") || productCategory.equals("2") || productCategory.equalsIgnoreCase("fruits")) {
            chooseProductFromArray(Products.fruits);
        } else if (productCategory.equals("3.") || productCategory.equals("3") || productCategory.equalsIgnoreCase("dairy")) {
            chooseProductFromArray(Products.dairy);
        }
        /*else {
            System.out.println("Category not found!"); //error for now as after this it still asks to provide
        }*/
    }

    public void chooseProductFromArray(Product[] productArray) {
        Product product = null;
        while (product == null) {
            String productListText = "";
            for (int i = 0; i < productArray.length; i++) {
                productListText = productListText + (i + 1) + ". " + productArray[i].name + " | ";
            }
            System.out.println(productListText);

            Scanner inputProduct = new Scanner(System.in);
            System.out.println(Main.ANSI_PURPLE + "Please choose product from the category: " + Main.ANSI_RESET);
            // String productNumber= inputProduct.nextLine();
            String inputProductText = inputProduct.nextLine();
            if (inputProductText.length() == 1 && Character.isDigit(inputProductText.charAt(0))) {
                int productIndex = Integer.parseInt(inputProductText);
                if (productIndex > 0 && productIndex <= productArray.length) {
                    product = productArray[productIndex - 1];
                }
            } else {
                for (int a = 0; a < productArray.length; a++) {
                    String productFromArrayName = productArray[a].name;
                    if (productFromArrayName.equalsIgnoreCase(inputProductText)) {
                        product = productArray[a];
                        break;
                    }
                }
            }
            if (product == null) {
                System.out.println(Main.ANSI_RED + "The product in the category not found!");
            }
        }
        selectProductDetails(product);
    }

    public void selectProductDetails(Product product) {
        System.out.println("You have selected " + product.name + " " + product.price + " EUR");

        Scanner inputAmount = new Scanner(System.in);
        System.out.println(Main.ANSI_PURPLE + "Please set the amount of your chosen products: " + Main.ANSI_RESET);
        //int productAmount = inputAmount.nextInt();

        String productAmount = inputAmount.nextLine();
        if (!productAmount.matches("[0-9]+")) {
            System.out.println("Please provide amount only in numbers");
            selectProductDetails(product);
            return;
        }

        int productAmountNumber = Integer.parseInt(productAmount);
        float priceSum = productAmountNumber * product.price;

        String formattedPriceSum = String.format("%.02f", priceSum);

        String continuationChoice1 = "";
        while (!continuationChoice1.equalsIgnoreCase("Yes") && !continuationChoice1.equalsIgnoreCase("No") && !continuationChoice1.equalsIgnoreCase("checkout")) {
            System.out.println("Your chosen purchase: " + " " + productAmount + " x " + product.name + " " + formattedPriceSum + " EUR");
            System.out.println(Main.ANSI_PURPLE + "Would you like to add this to your basket, look for other products or pay?" + Main.ANSI_RESET);
            System.out.println("Enter " + Main.ANSI_GREEN + "'YES'" + Main.ANSI_RESET + " to add: ");
            System.out.println("Enter " + Main.ANSI_YELLOW + "'NO'" + Main.ANSI_RESET + " to look for other product: ");
            System.out.println("Enter " + Main.ANSI_BLUE + "'Checkout'" + Main.ANSI_RESET + " to proceed with payment: ");
            Scanner input1 = new Scanner(System.in);
            continuationChoice1 = input1.nextLine();

            if (continuationChoice1.equalsIgnoreCase("yes")) {
                Purchase purchase = new Purchase(product, productAmountNumber);
                purchaseArrayList.add(purchase);
                totalSum = totalSum + priceSum;
            } else if (continuationChoice1.equalsIgnoreCase("no")) {
                startProductSelection();
                return;
            } else if (continuationChoice1.equalsIgnoreCase("checkout")) {
                if (purchaseArrayList.size() == 0) {
                    System.out.println("You have not added any product to you basket. Please select and add products to continue to checkout!!");
                    startProductSelection();
                }
                return;
            } else {
                System.out.println(Main.ANSI_RED + "The action you entered is not found. Please try again:" + Main.ANSI_RESET);
            }
        }

        System.out.println(getBasketTextToPrint());

        askHowToContinue();
    }

    public void askHowToContinue() {
        System.out.println(Main.ANSI_PURPLE + "How would you like to continue?" + Main.ANSI_RESET);
        System.out.println("Enter " + Main.ANSI_GREEN + "'YES'" + Main.ANSI_RESET + " to continue shopping: ");
        System.out.println("Enter " + Main.ANSI_YELLOW + "'NO'" + Main.ANSI_RESET + " to view all products in your basket: ");
        System.out.println("Enter " + Main.ANSI_BLUE + "'Checkout'" + Main.ANSI_RESET + " to proceed with payment: ");
        Scanner input2 = new Scanner(System.in);
        String continuationChoice2 = input2.nextLine();

        if (continuationChoice2.equalsIgnoreCase("Yes")) {
            startProductSelection();
        } else if (continuationChoice2.equalsIgnoreCase("NO")) {
            System.out.println(getBasketTextToPrint());
            System.out.println();
            // could call out askHowToContinue, however that would ask the use again if he wants to check the basket
            String continuationChoice3 = "";
            while (!continuationChoice3.equalsIgnoreCase("Yes") && !continuationChoice3.equalsIgnoreCase("checkout")) {
                System.out.println(Main.ANSI_PURPLE + "How would you like to continue?" + Main.ANSI_RESET);
                System.out.println("Enter " + Main.ANSI_GREEN + "'YES'" + Main.ANSI_RESET + " to continue shopping: ");
                System.out.println("Enter " + Main.ANSI_BLUE + "'Checkout'" + Main.ANSI_RESET + " to proceed with payment: ");
                Scanner inputChoice3 = new Scanner(System.in);
                continuationChoice3 = inputChoice3.nextLine();
                if (!continuationChoice3.equalsIgnoreCase("Yes") && !continuationChoice3.equalsIgnoreCase("checkout")) {
                    System.out.println(Main.ANSI_RED + "The selected operation not found!" + Main.ANSI_RESET);
                }
            }
            if (continuationChoice3.equalsIgnoreCase("Yes")) {
                startProductSelection();
            } else if (continuationChoice3.equalsIgnoreCase("Checkout")) {
                // executions goes to main to continue with next method
            }
        } else if (continuationChoice2.equalsIgnoreCase("Checkout")) {
            // executions goes to main to continue with next method
        } else {
            System.out.println(Main.ANSI_RED + "The selected operation not found!" + Main.ANSI_RESET);
            askHowToContinue();
        }
    }

    public String getBasketTextToPrint() {
        String result = "";

        DecimalFormat df = new DecimalFormat("##.##");

        result += "Basket:\n";

        for (int i = 0; i < purchaseArrayList.size(); i++) {
            Purchase purchaseInBasket = purchaseArrayList.get(i);
            float sum = purchaseInBasket.amount * purchaseInBasket.product.price;
            result += "  " + purchaseInBasket.amount + " x " + purchaseInBasket.product.name + " " + df.format(sum) + " EUR\n";
        }

        result += "Total sum: " + df.format(totalSum) + " EUR\n";

        return result;
    }
}
