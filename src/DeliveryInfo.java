import java.util.ArrayList;
import java.util.Scanner;

public class DeliveryInfo {
    String deliveryCity = "";
    String deliveryAddress = "";
    String contactPerson = "";
    String phoneNo = "";

    public void startDeliveryInfo() {
        System.out.println(Main.ANSI_BLUE  + "Dear Customer! We provide only fast delivery.\nNext Day Delivery ---> We will deliver your order within 24 hours");
        System.out.println(Main.ANSI_RESET + "DELIVERY INFORMATION:");
        Scanner input = new Scanner(System.in);
        System.out.println(Main.ANSI_PURPLE  + "1. Please write Your city: ");
        while (true) {

            deliveryCity = input.nextLine();
            if (!deliveryCity.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)")) {
                System.out.println(Main.ANSI_RED + "Something not correct, please check and write Your city again");
                continue;
            }
            System.out.println("OK, your City is: " + deliveryCity);
            System.out.println("------------------------------");
            System.out.println(Main.ANSI_PURPLE + "2. Please write Street name: ");

            while (true) {
                String deliveryAddress1 = input.nextLine();
                if (!deliveryAddress1.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)")) {
                    System.out.println(Main.ANSI_RED + "Something not correct, please check and write again");
                    continue;
                }
                    System.out.println("OK, your Street name: " + deliveryAddress1);
                System.out.println("------------------------------");
                System.out.println(Main.ANSI_PURPLE  +"3. Please write house number:");
                while (true) {
                    String deliveryAddress2 = input.nextLine();
                    if (!deliveryAddress2.matches("(.*[0-9].*)")) {
                        System.out.println(Main.ANSI_RED +"Please, maybe do you have also digits at your address? If Yes, please check again and write correct");//could be house number 13a
                        continue;
                    }
                    System.out.println("OK, Your House number " + deliveryAddress2);
                    System.out.println("------------------------------");
                    System.out.println(Main.ANSI_PURPLE  +"4. Please write flat number? (If you don't have flat number, please write '0')");
                    while (true) {
                        String deliveryAddress3 = input.nextLine();
                        if (!deliveryAddress3.matches("(.*[0-9].*)")) {
                            System.out.println(Main.ANSI_RED + "Please, maybe do you have also digits at your address? If Yes, please check again and write correct");//could be flat number 13a
                            continue;
                        }
                        System.out.println("OK, Your Flat number " + deliveryAddress3);
                        deliveryAddress = "Street " + deliveryAddress1 + " * House No: " + deliveryAddress2 + " * Flat No: " + deliveryAddress3;
                        System.out.println("------------------------------");
                        System.out.println(Main.ANSI_PURPLE  + "5. Please write Your Phone number (8-digit phone numbers):");
                        while (true) {
                            phoneNo = input.nextLine();
                            if (!phoneNo.matches("[0-9]{8}")) {
                                System.out.println(Main.ANSI_RED + "Please check Your number");
                                continue;
                            }
                            System.out.println(Main.ANSI_PURPLE  + "Yes, correct phone number: " + phoneNo);
                            System.out.println("------------------------------");
                            System.out.println(Main.ANSI_PURPLE + "6. Please write contact person for delivery: ");
                            while (true) {

                                contactPerson = input.nextLine();
                                if (!contactPerson.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)")) {
                                    System.out.println(Main.ANSI_RED +"Something not correct, please check and write again");
                                    continue;
                                }
                                System.out.println("------------------------------");
                                System.out.println(Main.ANSI_BLUE  + "DELIVERY INFORMATION: " +"\n" + "* Delivery city: " + deliveryCity +"\n" + "* Delivery address: " + deliveryAddress +"\n" + "* Phone number: " + phoneNo +"\n" + "* Contact person: " + contactPerson);
                                System.out.println("------------------------------");
                                break;
                            }
                            break;
                        }
                        break;
                    }
                    break;
                }
                break;
            }
            break;
        }
    }
}
