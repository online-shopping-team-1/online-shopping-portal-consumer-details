import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class WriteInFile {

    public static void write(String fileName, String textToAdd) {
        try {
            File file = new File("src/" + fileName);
            java.io.FileWriter fr = new java.io.FileWriter(file, true);
            fr.write(System.lineSeparator() + textToAdd);
            fr.close();
        }
        catch (IOException e) {
            System.out.println("Cannot write to file.");
            e.printStackTrace();
        }
    }
}