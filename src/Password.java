import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Password {

    public static boolean isLogin() throws IOException {
        Scanner input = new Scanner(System.in);

        int totalAttemptsLeft = 3;
        boolean credentialsValid = false;

        while (!credentialsValid) {

            System.out.println(Main.ANSI_PURPLE +"Please enter Username: ");
            String inputUsername = input.next();

            boolean userExists = doesUserExists(inputUsername);
            if (!userExists) {
                System.out.println("User with following username not found!");

                System.out.println("Do you want to register new user?: ");
                String inputAnswer = input.next();
                if (inputAnswer.equalsIgnoreCase("Yes")) {
                    newUserRegistration();
                    continue;
                } else {
                    System.out.println("Good buy! Have a nice day!");
                    break;
                }
            }

            System.out.println(Main.ANSI_PURPLE + "Please enter Password: ");
            String inputPassword = input.next();

            credentialsValid = isCredentialValid(inputUsername, inputPassword);
            totalAttemptsLeft--;

            if (!credentialsValid) {
                if (totalAttemptsLeft <= 0) {
                    System.out.println(Main.ANSI_RED + "You have entered wrong three times. Please try again in a five minutes");
                    System.exit(0);
                } else {
                    System.out.println(Main.ANSI_RED + "Wrong credentials. Attention! You have " + totalAttemptsLeft + " attempts to logging. Please try again!");
                }
            }
        }

        return credentialsValid;
    }

    public static boolean doesUserExists(String userName) {
        ArrayList<String> fileLines = ReadFile.readFile("userNames");

        boolean userFound = false;
        for (int i = 0; i < fileLines.size(); i++) {
            String line = fileLines.get(i);
            String[] splittedLineBySpace = line.split(" ");
            String usernameFromFile = splittedLineBySpace[0];

            if (usernameFromFile.equals(userName)) {
                userFound = true;
                break;
            }
        }

        return userFound;
    }

    public static boolean isCredentialValid(String userName, String password) {
        ArrayList<String> fileLines = ReadFile.readFile("userNames");

        boolean credentialsValid = false;

        for (int i = 0; i < fileLines.size(); i++) {
            String line = fileLines.get(i);
            String[] splittedLineBySpace = line.split(" "); //Works only if when registering user in the file the credentials are written and sepperated/splitted by space
            if (splittedLineBySpace.length >= 2) {
                String usernameFromFile = splittedLineBySpace[0];
                String passwordFromFile = splittedLineBySpace[1];

                if (usernameFromFile.equals(userName) && passwordFromFile.equals(password)) {
                    credentialsValid = true;
                    break;
                }
            }
        }
        return credentialsValid;
    }

    public static void newUserRegistration() {

        System.out.println("Please enter new user name: ");
        Scanner input = new Scanner(System.in);
        String inputUsernameNew = input.nextLine();
        System.out.println(inputUsernameNew);
        System.out.println("Please create a password: \n " + "* Password should be less than 17 and more than 9 characters in length.\n" +
                " * Password should contain at least one upper case and one lower case alphabet.\n" +
                " * Password should contain at least one number.\n" +
                " * Password should contain at least one special character.");

        while (true) {
            String passwordNew = input.nextLine();
            System.out.println(passwordNew);
            if (passwordNew.length() > 17 || passwordNew.length() < 9) {
                System.out.println("Password should be less than 17 and more than 9 characters");
                continue;
            }
            String upperCaseChars = "(.*[A-Z].*)";//.* meaning= any character sequence, any number of digits, any number of letter...
            if (!passwordNew.matches(upperCaseChars)) {
                System.out.println("Password should contain at least one upper case alphabet");
                continue;
            }
            String lowerCaseChars = "(.*[a-z].*)";//.* meaning= any character sequence, any number of digits, any number of letter...
            if (!passwordNew.matches(lowerCaseChars)) {
                System.out.println("Password should contain at least one lower case alphabet");
                continue;
            }
            String numbers = "(.*[0-9].*)";//.* meaning= any character sequence, any number of digits, any number of letter...

            if (!passwordNew.matches(numbers)) {
                System.out.println("Password should contain at least one number.");
                continue;
            }
            String specialChars = "(.*[,~,!,@,#,$,%,^,&,*,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?].*$)";
            if (!passwordNew.matches(specialChars)) {
                System.out.println("Password should contain at least one special character");
                continue;
            }
        System.out.println("Password is valid. Thank you! Now You can login with Your user name and password");
        WriteInFile.write("userNames", inputUsernameNew+" " + passwordNew);
        break;
        }
}}