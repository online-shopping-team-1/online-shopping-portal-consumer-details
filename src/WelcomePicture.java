import javax.swing.*;
import java.awt.*;

class WelcomePicture extends JPanel

{
    public WelcomePicture()
    {

        ImageIcon imageIcon = new ImageIcon(this.getClass().getResource("welcome.jpg"));
        JLabel label = new JLabel(imageIcon);
        add(label);
    }

    public void showWelcomePicture()
    {
        JFrame frame = new JFrame();
        frame.setPreferredSize(new Dimension(800, 500));
        frame.pack();
        frame.add(new WelcomePicture());
        frame.setVisible(true);
    }
}
