import java.io.IOException;

public class Main {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_WHITE = "\u001B[37m";


    public static void main(String[] args) throws IOException {
        WelcomePicture welcomePicture = new WelcomePicture();
        welcomePicture.showWelcomePicture();
        System.out.println("Welcome to our online store!");

        System.out.println("****************************");

        System.out.println(" ");
        boolean isUserLoggedIn = Password.isLogin();
        if (isUserLoggedIn) {
            System.out.println(ANSI_RESET + "You are now logged in");
            Basket basket = new Basket();
            basket.startProductSelection();
            System.out.println(" ");
            DeliveryInfo deliveryInfo = new DeliveryInfo();
            deliveryInfo.startDeliveryInfo();
            Invoice1 invoice = new Invoice1(basket, deliveryInfo);
            invoice.printInvoice();
        }
    }
}






