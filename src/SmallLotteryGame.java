
import java.util.Random;
import java.util.Scanner;

public class SmallLotteryGame {

    public static double startLotteryGame() {
        System.out.println(Main.ANSI_YELLOW + "Please play a little game and get a prize for shopping: ");
        System.out.println("Please enter 5 digits from 0 to 9: ");

        int NUM_DIGITS = 5;
        double sameNum;

        int[] userDigits = new int[5];
        int[] lotteryNumbers = new int[5];

        generateNumbers(lotteryNumbers);
        getUserData(userDigits);
        sameNum = compareArrays(userDigits, lotteryNumbers);

        System.out.println("Lottery numbers: " + lotteryNumbers[0] + " " +
                lotteryNumbers[1] + " " + lotteryNumbers[2] + " " + lotteryNumbers[3] +
                " " + lotteryNumbers[4] + " ");

        System.out.println("Your numbers: " + userDigits[0] + " " + userDigits[1] + " " + userDigits[2] + " " + userDigits[3] + " " + userDigits[4] + " ");
        System.out.println("Number of matching digits: " + sameNum);

        if (sameNum ==0){
            System.out.println("No matching numbers :( better luck next time");
            System.out.println("");
        }
        if (sameNum<=5 && sameNum>=1){
            System.out.println("GIFT CARD " + sameNum + " EUR");
        }
//        if (sameNum == 5){
//            System.out.println("Gift card 5 eur");

        return sameNum;
    }
    public static int generateNumbers(int [] lotteryNumbers){

        Random randNum = new Random();

        lotteryNumbers[0] = randNum.nextInt(10);
        lotteryNumbers[1] = randNum.nextInt(10);
        lotteryNumbers[2] = randNum.nextInt(10);
        lotteryNumbers[3] = randNum.nextInt(10);
        lotteryNumbers[4] = randNum.nextInt(10);

        return lotteryNumbers[4];
    }

    public static int getUserData (int [] userDigits){
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Write number 1: ");
        userDigits[0] = keyboard.nextInt();
        System.out.print("Write number 2: ");
        userDigits[1] = keyboard.nextInt();
        System.out.print("Write number 3: ");
        userDigits[2] = keyboard.nextInt();
        System.out.print("Write number 4: ");
        userDigits[3] = keyboard.nextInt();
        System.out.print("Write number 5: ");
        userDigits[4] = keyboard.nextInt();
        return userDigits[4];
    }

    public static int compareArrays (int [] userDigits,
                                     int [] lotteryNumbers)
    {
        int sameNum = 0;
        for (int i = 0; i < 5; i++){

            if (lotteryNumbers[i] == userDigits[i])
            {
                sameNum++;
            }
        }
        return sameNum;
    }
}
