import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile {
    public static ArrayList<String> readFile(String fileName) {
        ArrayList<String> fileLines = new ArrayList<>();

        try {
            File myFile = new File("src/" + fileName);
            Scanner myReader = new Scanner(myFile);
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                fileLines.add(line);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File cant be read.");
            e.printStackTrace();
        }

        return fileLines;
    }
}