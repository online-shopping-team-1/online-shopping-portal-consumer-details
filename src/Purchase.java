public class Purchase {

    Product product;
    int amount;

    public Purchase(Product product, int amount) {
        this.product = product;
        this.amount = amount;
    }
}
